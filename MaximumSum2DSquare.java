/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author yogesh
 */
public class MaximumSum2DSquare {
    int row;
    int col;
    int squareSize;
    int[][] array;
    
    int sum = 0;
    int counter = 0;
        
    MaxResult maxResult;
    MinResult minResult;
        
    
    public MaximumSum2DSquare(int row, int col, int squareSize){
        this.row = row;
        this.col = col;
        this.squareSize = squareSize;
        this.array = new int[row][col];
        this.maxResult = new MaxResult();
        this.minResult = new MinResult();
    }
    
    public void initializeArray() throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        for(int r = 0; r < row; r++){
            for(int c = 0; c < col; c++){
                System.out.println("Please enter element at array["+r+"]"+"["+c+"]");
                int n = Integer.parseInt(reader.readLine());
                this.array[r][c] = n;
            }
        }
        this.printArray(row, col);
    }
    
    public void calculate(){
        for(int r = 0; r < this.row; r++){
            for(int c = 0; c < this.col; c++){
                if(r + (this.squareSize - 1) <= (this.row - 1) && (c + (this.squareSize - 1)) <= (this.col - 1)){
                    this.findSum(r, c);
                    counter++;
                }
            }
        }
        
        this.printResult();
    }
    
    void findSum(int start_row, int start_col){
           
        this.sum += this.array[start_row][start_col];
        
        for( int i = 1; i < this.squareSize; i++){
            this.sum += this.array[start_row + i][start_col];
            this.sum += this.array[start_row][start_col + i];
            this.sum += this.array[start_row + i][start_col + i];
            
            for(int j=1; j < i; j++){
                this.sum += this.array[(start_row + i) - j][(start_col + i)];
                this.sum += this.array[(start_row + i)][(start_col + i) - j];
            }
           
        }
         //Get max sum square location
        if(this.sum > this.maxResult.squareMaxSum){
            this.maxResult.squareMaxSum = this.sum;
            this.maxResult.maxSquareStartRow = start_row;
            this.maxResult.maxSqureStartCol = start_col;
        }
            
        //Get min sum square location
        if(this.sum < this.minResult.squareMinSum){
            this.minResult.squareMinSum = this.sum;
            this.minResult.minSquareStartRow = start_row;
            this.minResult.minSqureStartCol = start_col;
        }
        //Reset sum for next sqaure calculation
        this.sum = 0;
    }
    
    public void printArray(int row, int col){
        
        System.out.println("2d Array: ");
        System.out.println();
        
        for(int r = 0; r < row; r++){
            for(int c = 0; c < col; c++){
                System.out.print(this.array[r][c]);
                if(c == (col - 1))
                    System.out.println();
            }
        }
        System.out.println();
    }
    
    public void printResult(){
        System.out.println("#############  Max square result ##############");
        this.maxResult.printResult();
        
        System.out.println();
        for(int r = maxResult.maxSquareStartRow; r < (maxResult.maxSquareStartRow + this.squareSize); r++){
            for(int c = maxResult.maxSqureStartCol; c < (maxResult.maxSqureStartCol + this.squareSize); c++){
                System.out.print(this.array[r][c]);
                if(c == (maxResult.maxSqureStartCol + this.squareSize - 1))
                    System.out.println();
            }
        }
        
        System.out.println();
        System.out.println("############ Min square result #############");
        this.minResult.printResult();
        
        System.out.println();
        for(int r = minResult.minSquareStartRow; r < (minResult.minSquareStartRow + this.squareSize); r++){
            for(int c = minResult.minSqureStartCol; c < (minResult.minSqureStartCol + this.squareSize); c++){
                System.out.print(this.array[r][c]);
                if(c == (minResult.minSqureStartCol + this.squareSize - 1))
                    System.out.println();
            }
        }
        
        System.out.println("Total squares: "+this.counter);
        
    }
    
    class MaxResult{
        public int squareMaxSum = 0;
        public int maxSquareStartRow = -1;
        public int maxSqureStartCol = -1;
        
        public void printResult(){
            System.out.println("Square max sum: "+squareMaxSum);
            System.out.println("Square max start row: "+maxSquareStartRow);
            System.out.println("Square max start col: "+maxSqureStartCol);
     
        }
        
    }
    
    class MinResult{
        public int minSquareStartRow = -1;
        public int minSqureStartCol = -1;
        public int squareMinSum = 99999;
        
        public void printResult(){
            System.out.println("Square min sum: "+squareMinSum);
            System.out.println("Square min start row: "+minSquareStartRow);
            System.out.println("Square min start col: "+minSqureStartCol);

        }
    }
}
