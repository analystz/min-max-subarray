# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Program to find min and max inside a 2d array. Given a 2D array, find 4x4 subarray having max and min sums

### How do I get set up? ###

* Use any Java IDE and copy paste the code after setting up the project!
* Create a new package called algorithms
* Add both files inside that package
* Algorithm class is the main entry point. Parameters to MaximumSum2DSquare(rows, cols, squareSize)
* Where rows is the total rows of 2D array
* cols is the total columns of 2D array
* squareSize is the size of subarray to find having min and max sums 

